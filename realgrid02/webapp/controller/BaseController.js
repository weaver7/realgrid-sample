sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent",
    "sap/ui/model/json/JSONModel"
],
function (Controller, UIComponent, JSONModel) {
    "use strict";

  return Controller.extend("com.istn.realgrid02.controller.BaseController", {
    onInit: function () {
    },

    getRouter: function(){
        return UIComponent.getRouterFor(this);
    }
  });
});
