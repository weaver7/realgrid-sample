sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel"
],
	/**
	 * @param {typeof sap.ui.core.mvc.Controller} Controller
	 */
	function (Controller, JSONModel) {
		"use strict";

		return Controller.extend("com.istn.realgrid02.controller.App", {
			onInit: function () {

			},

			onAfterRendering: function () {
				var that = this;
				this._createRealGrid();
				this._getAccessTokenTestService().then(function (response) {
					var accessToken = response.token;
					return that._getSampleData(accessToken);
				}).then(function (response) {
					console.log("completed _getSampleData");
				});
			},

			// NodeJS Access Token 발급 (middleware 서버가 없는 경우 무시)
			_getAccessTokenTestService: function () {
				return $.ajax({
					url: "/TestService/api/v1/oauth/token",
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					data: JSON.stringify({
						"x-istn-client-id": "SVNUTl9JTlRFUk5BTF9ERVZfS0VZ",
						"x-istn-client-secret": "0BZEHaInDrJP3dOuQUoiSdvD"
					}),
					success: function (data) {
						return data;
					},
					error: function (error) {
						console.log(error);
					}
				})
			},

			_getSampleData: function (accessToken) {
				var that = this;
				var view = this.getView();
				return $.ajax({
					url: "/TestService/api/v1/taxinvoice/sales_rg.list",
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"x-auth-token": "Bearer " + accessToken
					},
					data: JSON.stringify({
						subdomain: "istnci",
						business_place: [],
						customer: [],
						from_date: "20170101",
						to_date: "20200101"
					}),
					success: function (data) {
						var realgrid = view.getModel("realgrid").getData();
            view.setModel(new JSONModel(data.value), "testModel");
						var dataProvider = realgrid.provider;
						var gridView = realgrid.gridView;
						gridView.setDataSource(dataProvider);
						dataProvider.setRows(data.value);
					},
					error: function (error) {
						console.log(error);
					}
				});
			},

			_createRealGrid() {
				try {
					var view = this.getView();
					var gridComponent = view.byId("id--realgrid");
					var container = gridComponent.getDomRef();
					var provider = new RealGrid.LocalDataProvider(false);
					var gridView = new RealGrid.GridView(container);
					gridView.setDataSource(provider);

					//* Field 설정
					provider.setFields([
						{
							fieldName: 'RBUKRS',
							dataType: 'text',
						},
						{
							fieldName: 'GJAHR',
							dataType: 'text',
						},
						{
							fieldName: 'BELNR',
							dataType: 'text',
						},
						{
							fieldName: 'DOCLN',
							dataType: 'text',
						},
						{
							fieldName: 'AWREF',
							dataType: 'text',
						},
						{
							fieldName: 'KOART',
							dataType: 'text',
						},
						{
							fieldName: 'TSL',
							dataType: 'text',
						},
						{
							fieldName: 'RTCUR',
							dataType: 'text',
						},
						{
							fieldName: 'NETWR',
							dataType: 'text',
						},
						{
							fieldName: 'MWSBK',
							dataType: 'text',
						},
						{
							fieldName: 'SELR_CORP_NM',
							dataType: 'text',
						},
						{
							fieldName: 'SELR_CORP_NO',
							dataType: 'text',
						},
						{
							fieldName: 'SELR_CEO',
							dataType: 'text',
						}
					]);

					//* Column 설정
					gridView.setColumns([
						{
							name: 'RBUKRS',
							fieldName: 'RBUKRS',
							type: 'data',
							width: '70',
							header: {
								text: '회사코드',
							},
						},
						{
							name: 'GJAHR',
							fieldName: 'GJAHR',
							type: 'data',
							width: '70',
							header: {
								text: '회계연도',
							},
						},
						{
							name: 'BELNR',
							fieldName: 'BELNR',
							type: 'data',
							width: '80',
							header: {
								text: '전표',
							},
						},
						{
							name: 'DOCLN',
							fieldName: 'DOCLN',
							type: 'data',
							width: '50',
							header: {
								text: '항목',
							},
						},
						{
							name: 'AWREF',
							fieldName: 'AWREF',
							type: 'data',
							width: '80',
							header: {
								text: '참조',
							},
						},
						{
							name: 'KOART',
							fieldName: 'KOART',
							type: 'data',
							width: '40',
							header: {
								text: '구분',
							},
						},
						{
							name: 'TSL',
							fieldName: 'TSL',
							type: 'data',
							width: '100',
							header: {
								text: '총액',
							},
							styles: {
								textAlignment: 'far',
								iconLocation: 'left',
								iconAlignment: 'center',
								iconOffset: 4,
								iconPadding: 4,
							}
						},
						{
							name: 'RTCUR',
							fieldName: 'RTCUR',
							type: 'data',
							width: '60',
							header: {
								text: '통화',
							},
						},
						{
							name: 'NETWR',
							fieldName: 'NETWR',
							type: 'data',
							width: '100',
							header: {
								text: '공급가액',
							},
						},
						{
							name: 'MWSBK',
							fieldName: 'MWSBK',
							type: 'data',
							width: '100',
							header: {
								text: '세액',
							},
						},
						{
							name: 'SELR_CORP_NM',
							fieldName: 'SELR_CORP_NM',
							type: 'data',
							width: '100',
							header: {
								text: '회사명',
							},
						},
						{
							name: 'SELR_CORP_NO',
							fieldName: 'SELR_CORP_NO',
							type: 'data',
							width: '70',
							header: {
								text: '사업자번호',
							},
						},
						{
							name: 'SELR_CEO',
							fieldName: 'SELR_CEO',
							type: 'data',
							width: '70',
							header: {
								text: '대표자명',
							},
						}
					]);

					// provider, gridView 를 모델에 저장
					this.getView().setModel(new JSONModel({ provider, gridView }), "realgrid");
				} catch (error) {
					debugger;
					console.log(error);
				}
			}
		});
	});
